#!/bin/sh
# touchpad_toggle
#
# Toggle the touchpad on/off.

schema=org.gnome.desktop.peripherals.touchpad
key=send-events
# find out whether the touchpad is enabled or not
touchpad_status=$(gsettings get "$schema" "$key")
if [ "$touchpad_status" = "'enabled'" ]; then
	# The touchpad is currently enabled, so turn it off.
	gsettings set "$schema" "$key" "'disabled'"
	echo "Touchpad disabled"
elif [ "$touchpad_status" = "'disabled'" ]; then
	# The touchpad is currently disabled, so turn it on.
	gsettings set "$schema" "$key" "'enabled'"
	echo "Touchpad enabled"
else
	echo "Touchpad toggle: Unknown touchpad status $touchpad_status"
	echo "  obtained from $schema $key"
	exit 1
fi
