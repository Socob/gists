<?php
namespace de\uni_muenster\fsphys;
require_once 'error_handler.inc';
require_once 'db_access.inc';

/*
	Determines if the database table given by $table_name exists.
	
	Returns true if the table exists, false otherwise.
	Throws DBException on query errors.
*/
function table_exists($table_name, $db=NULL) {
	$sql = <<<'SQL'
	SELECT * FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = :table_name;
SQL;
	$opened_db = mysql_db_connect_if_null($db);
	$query = $db->prepare($sql);
	$query->bindValue(':table_name', $table_name);
	$query->execute();
	$result = $query->fetch();
	if ($opened_db) {
		mysql_db_close($db);
	}
	if ($result === false) {
		throw new fsphys\DBException("Querying for existence of database "
			. "table $table_name failed");
	}
	return count($result) > 0;
}
