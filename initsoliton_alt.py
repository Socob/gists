#!/usr/bin/env python3
import numpy

BOX_LENGTH = 8
RESOLUTION = 256
DURATION = 0.4
X_SEP = 4
V_REL = 20
T_0 = X_SEP / V_REL
# used soliton parameters
SOLITON_1 = [ 5, [ X_SEP/2, 0, 0], [-V_REL/2, 0, 0], 0]
SOLITON_2 = [10, [-X_SEP/2, 0, 0], [ V_REL/2, 0, 0], 0]
# constants for plot of theoretical soliton profile
SOLITON_PATH = 'initial_f.npy'
# resolution of soliton profile array file
SOLITON_DELTA_X = 0.00001
# α = m²/3.883²
SOLITON_ALPHA_M = 1 / 3.883**2
SOLITON_BETA = 2.454
SOLITON_MAX_RADIUS = 5.6

distances = (
	(numpy.arange(-RESOLUTION / 2, RESOLUTION / 2) + 1/2) *
	BOX_LENGTH / RESOLUTION
)
# calculate theoretical profile
# taken from PyUltraLight (should be identical to distances)
gridvec = numpy.linspace(
	-BOX_LENGTH / 2 + BOX_LENGTH / (2 * RESOLUTION),
	BOX_LENGTH / 2 - BOX_LENGTH / (2 * RESOLUTION),
	RESOLUTION
)
# get parameters
m_1, x_1, v_1, _ = SOLITON_1
x_1 = numpy.array(x_1)
v_1 = numpy.array(v_1)
delta = SOLITON_1[-1] - SOLITON_2[-1]
alpha_1 = m_1**2 * SOLITON_ALPHA_M

# construct f(√α_1 x)
xvals, yvals, zvals = numpy.meshgrid(
	(gridvec - x_1[0])**2, (gridvec - x_1[1])**2, (gridvec - x_1[2])**2,
	indexing='ij'
)
dist_from_center = numpy.sqrt(xvals + yvals + zvals)
f_initial = numpy.load(SOLITON_PATH)
# f(√α_1 x)
indices = (numpy.sqrt(alpha_1) * (dist_from_center / SOLITON_DELTA_X + 1)).astype(int)
indices[indices >= len(f_initial)] = -1
f = alpha_1 * f_initial[indices]
f[numpy.sqrt(alpha_1) * dist_from_center > SOLITON_MAX_RADIUS] = 0
