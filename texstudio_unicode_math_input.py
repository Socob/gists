#!/usr/bin/env python3
import csv
import json
import subprocess
from pathlib import Path

# dict of LaTeX command → Unicode code point
commands = []
# parse data from unicode-math-table.tex
path = subprocess.check_output(['kpsewhich', 'unicode-math-table.tex'],
	encoding='utf-8').strip('\n')
with open(path, 'rt', newline='') as f:
	csv = csv.reader(f, delimiter='{', quoting=csv.QUOTE_NONE)
	for row in csv:
		if not row or row[0].startswith('%'):
			continue
		row = [x.strip(' \t\n\\"%}') for x in row]
		commands.append({
			'char': chr(int(row[1], base=16)),
			'command': row[2],
			'group': row[3],
			'description': row[4],
		})
# assign macro triggers for unicode-math-table.tex
replacements = []
for item in commands:
	command = item['command']
	command_triggers = [command]
	if command.startswith('mup'):
		command_triggers.append(command[3:])
	char = item['char']
	for trigger in command_triggers:
		if  '\\' in char or '"' in char:
			continue
		replacements.append(
			fr'    "\{trigger} ": "{char}",'
		)
# write macro file
macro = json.dumps({
	'name': 'unicode-math',
	'tag': [
		'%SCRIPT',
		'replacements = {',
	] +
	replacements +
	[
		'}',
		'c = triggerMatches',
		'if (c in replacements) {',
		'    c = replacements[c]',
		'}',
		'editor.write(c)',
	],
	'description': [
		'Replace control sequences from unicode-math-table.tex with '
			'Unicode characters',
	],
	'abbrev': '',
	'trigger': r'\\\w+ ',
	'menu': '',
	'shortcut': '',
	# needed for current TexStudio master, but breaks TeXStudio 2.12.14
#		'formatVersion': 1,
}, ensure_ascii=False, indent=4).replace(
	# TeXStudio reads escaped backslashes in JSON literally,
	# i.e. \\ is \\ instead of \
	r'"trigger": "\\\\\\w+ ",', r'"trigger": "\\\w+ ",'
)
out_p = Path() / 'unicode.txsMacro'
out_p.write_text(macro)
print(f'Wrote {len(replacements)} macro triggers')