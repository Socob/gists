#!/usr/bin/env python3
import base64
import email
import sys

mail_filenames = sys.argv[1:]

for filename in mail_filenames:
	with open(filename, 'rb') as mail_file:
		mail = email.message_from_binary_file(mail_file)
	for i, part in enumerate(mail.walk()):
		if part.get_content_type() == 'application/pdf':
			with open(filename + '.pdf', 'wb') as pdf:
				pdf.write(base64.b64decode(part.get_payload()))
