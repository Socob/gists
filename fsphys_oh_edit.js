document.addEventListener('DOMContentLoaded', function() {
	let MODIFY_CLASS = '<?php echo MODIFY_CLASS; ?>';
	let MODIFY_ID_PREFIX = '<?php echo MODIFY_ID_PREFIX; ?>';
	let MOUSEOVER_COLOR = '<?php echo MOUSEOVER_COLOR; ?>';
	// get all table cells which can be modified
	let cells = document.querySelectorAll(`td.${MODIFY_CLASS}`);
	for (let cell of cells) {
		cell.onmouseover = function () {
			this.style=`background-color: ${MOUSEOVER_COLOR};`;
		};
		cell.onmouseout = function () {
			this.style='';
		};
		let id = cell.id.replace(MODIFY_ID_PREFIX, '');
		cell.onclick = function() {
			let day_str = '';
			let parts = id.split('_');
			for (let part of parts) {
				if (part.startsWith('t-')) {
					part = part.substr(2);
					var [time_start, time_end] = part.split('-');
				}
				else if (part.startsWith('d-')) {
					let day = part.substr(2);
					day_str = `&amp;day=${day}`;
				}
			}
			let target = `?break=false&amp;start_time=${start_time}`
				+ `&amp;end_time=${end_time}${day_str}`;
			window.location.href = target;
		};
	}
});