#!/usr/bin/env python3
import sys

with open(sys.argv[1], 'rb') as f:
	data = f.read()
	sys.stdout.buffer.write(data[::-1])
