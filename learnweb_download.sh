#!/bin/sh
wget -r --no-parent --page-requisites --convert-links --backup-converted \
	--adjust-extension --https-only --level=15 --wait=1 \
	--exclude-directories \
'/LearnWeb/learnweb2/admin,'\
'/LearnWeb/learnweb2/backup,'\
'/LearnWeb/learnweb2/blog,'\
'/LearnWeb/learnweb2/calendar,'\
'/LearnWeb/learnweb2/enrol,'\
'/LearnWeb/learnweb2/filter,'\
'/LearnWeb/learnweb2/grade/edit,'\
'/LearnWeb/learnweb2/login,'\
'/LearnWeb/learnweb2/message,'\
'/LearnWeb/learnweb2/my,'\
'/LearnWeb/learnweb2/portfolio,'\
'/LearnWeb/learnweb2/repository,'\
'/LearnWeb/learnweb2/tag,'\
'/LearnWeb/learnweb2/user' \
	--regex-type=posix --reject-regex \
'.*delete.*|'\
'.*id=4789($|[^\d].*)|'\
'.*/LearnWeb/learnweb2/\?time=.*|'\
'.*/LearnWeb/learnweb2/course/.*bui_hideid=.*|'\
'.*/LearnWeb/learnweb2/course/.*bui_showid=.*|'\
'.*/LearnWeb/learnweb2/course/.*duplicate=.*|'\
'.*/LearnWeb/learnweb2/course/.*edit=on.*|'\
'.*/LearnWeb/learnweb2/course/.*hide=.*|'\
'.*/LearnWeb/learnweb2/course/.*indent=.*|'\
'.*/LearnWeb/learnweb2/course/index\.php\?categoryid=.*|'\
'.*/LearnWeb/learnweb2/course/(changenumsections|edit|info|mod)\.php.*|'\
'.*/LearnWeb/learnweb2/course/(modedit|switchrole)\.php.*|'\
'.*/LearnWeb/learnweb2/mod/(assign|glossary)/.*comment=.*|'\
'.*/LearnWeb/learnweb2/mod/(data|glossary|question|wiki)/edit\.php.*|'\
'.*/LearnWeb/learnweb2/mod/forum/(post|subscribe)\.php.*|'\
'.*/LearnWeb/learnweb2/mod/wiki/(create|editcomments|filesedit)\.php.*' \
	--http-user=<USER_NAME> --ask-password \
	'https://sso.uni-muenster.de/LearnWeb/learnweb2' \
	2>&1 | tee 'wget.log'
