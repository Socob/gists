#!/usr/bin/env python3
import email
import sys
from email.header import decode_header, make_header

mail_filenames = sys.argv[1:]

addresses = set()
for filename in mail_filenames:
	if filename.endswith('.py'): continue
	with open(filename, 'rb') as mail_file:
		mail = email.message_from_binary_file(mail_file)
	email_from = mail['From']
	if email_from:
		header = make_header(decode_header(email_from))
		addresses.add(str(header))
print(*addresses, sep='\n')
print(len(addresses))
